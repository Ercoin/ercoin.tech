# Ercoin’s web site

## Installation

1. Install [Haskell Stack](https://haskellstack.org).
2. `stack build`

## Building

```
stack exec site rebuild
```

## Live server with automatic reloading

```
stack exec site watch
```
