---
title: Initial Burn Offering
date: 2019-10-08
---
With the release of [Ercoin 1.0.0 beta](/news/ercoin-1.0.0-beta.1.html), it is time to announce Initial Burn Offering. The distribution of ercoins in the genesis block will be made proportionally to the amount of [blackcoins](https://blackcoin.org) burnt **from the beginning of 8th of November 2019 (UTC) until the end of 7th of December 2019 (UTC)** (block times are normative). **The minimum required burnt amount per Ercoin address is 3 BLK**. The total number of ercoins created will be 2.6 billion.

If everything goes well (which is by no means certain), the network will start on 15th of December (UTC). Before the real IBO, there will be a test IBO performed on BlackCoin testnet. See the [roadmap](#roadmap) for detailed schedule.

For detailed, technical rules, see the [README](https://gitlab.com/Ercoin/ercoin/blob/master/README.md) in the main Ercoin repository.

Community-provided IBO explorers: [ibo.laboratorium.ee](https://ibo.laboratorium.ee).

## How to participate

### Step 1: Generate an address

Open the Ercoin wallet and generate a new account. **Remember to back up the account.**

### Step 2: Create genesis transaction

Go to the “Genesis” tab in the wallet and create a genesis transaction. Do not select the “locked” option unless you know what you are doing.

### Step 3: Burn the coins

There are at least two methods: directly burn blackcoins or pay someone to do it for you. Genesis transaction has to be associated with the burnt coins. One transaction may be “burnt” into blackcoin blockchain multiple times.

**Regardless of the method, remember to fit into the time window (not too early, not too late)!** You may want to start the burning with small amounts, to give yourself an opportunity to get assured that everyting works as expected before burning significant amounts of money.

#### Method A: Burn the coins oneself

First get a [Blackcoin](https://blackcoin.org) wallet that faciliates coin burning[^wallet-non-burning] (“[More wallet](https://blackcoinmore.org/)” since version 2.13.2.4-067783fa70[^more-2.13.2.4] and “Original wallet” since version 1.2.5 should be good in this regard). Send the coins that you intend to burn to this address. Note that a wallet may need even a few days for initial sync!

When you have the coins sitting happily in your wallet, open “Help → Debug window → Console”. To burn the coins, type:

```
burn <amount> <genesis_transaction>
```

, where `<amount>` is the amount of blackcoins you want to burn and `<genesis_transaction>` is created in step 2.

#### Method B: Pay someone to burn the coins

To participate in the IBO, you need to create an unspendable transaction output which carries genesis transaction created in step 2 as data. Neither you nor anybody else will be able to spend blackcoins associated with this output (they will be “burnt”), so you don’t really need a full-fledged wallet. When buying blackcoins, you can thereore tell the seller to not send them to an ordinary address, but to burn them for you instead, providing him with the genesis transaction created in step two.

On most exchanges you purchase blackcoins and then specify a traditional withdrawal address. This is not flexible enough to allow withdrawals to burnt outputs. However decentralized exchange [Bisq](https://bisq.network/) has an asset named „burnt BlackCoin” (which can be found under the “sell BTC” tab).

## Participating in genesis validators’ election

Beware that validators are elected only by locked accounts (this is done to emphasize the role of long-term investors). If you decide to lock your account and find a competent provider of validator service, then he will be also competent enough to explain to you what to do to lock your account and vote (of course the reverse may be not true).

## Roadmap

All dates & times are given in UTC.

+-------------------+---------------------+
|2019-10-15 00:00:00|Test IBO start       |
+-------------------+---------------------+
|2019-10-21 23:59:59|Test IBO end         |
+-------------------+---------------------+
|2019-10-22 00:00:00|Test fee voting start|
+-------------------+---------------------+
|2019-10-28 00:00:00|Test fee voting end  |
+-------------------+---------------------+
|2019-10-29 00:00:00|Test genesis         |
+-------------------+---------------------+
|2019-11-08 00:00:00|IBO start            |
+-------------------+---------------------+
|2019-12-07 23:59:59|IBO end              |
+-------------------+---------------------+
|2019-12-08 00:00:00|Fee voting start     |
+-------------------+---------------------+
|2019-12-14 00:00:00|Fee voting end       |
+-------------------+---------------------+
|2019-12-15 00:00:00|Genesis              |
+-------------------+---------------------+

[^wallet-non-burning]: If your wallet does not faciliate coin burning, then it doesn’t mean that you won’t be able to burn the coins. This topic is however not covered in this document.
[^more-2.13.2.4]: If you downloaded BlackCoin More before 2019-11-12, you may have a problem with the wallet creating burn transactions, but not broadcasting them. It can be bypassed by setting the `-datacarriersize` option to a higher value (150 should be sufficient).
