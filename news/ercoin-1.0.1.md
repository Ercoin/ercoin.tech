---
title: Ercoin 1.0.1
date: 2020-01-29
---

Ercoin 1.0.1 has been released. This version fixes a bug which under some circumstances could cause old votes to reappear. Starting from the third epoch (third week of Ercoin operation), it may affect generated application hashes; nodes are encouraged to upgrade beforehand. Wallets should be unaffected.
