---
title: Press release for the IBO
date: 2019-11-03
---

[Initial Burn Offering of Ercoin](/news/ibo.html) has been announced. Ercoin is a cryptocurrency with fair distribution, delegated proof of locked stake used as a consensus algorithm and [Tendermint](https://tendermint.com) used as a consensus engine.

The **initial distribution** will be performed in a way that aims to be both **fair** (no entity privileged) and **green** (negligible CO₂ emission). Coins will be assigned proportionally to the amount of [blackcoins](https://blackcoin.org) destroyed (“burnt”) in a specific manner from the beginning of 8th of November 2019 (UTC) until the end of 7th of December 2019 (UTC). People outside the BlackCoin community will be able to **participate in the Initial Burn Offering by spending bitcoins** to purchase blackcoins that are already burnt, on the decentralized exchange [Bisq](https://bisq.network/).

Initial Burn Offering is similar in spirit to the way in which first cryptocurrencies were distributed (Bitcoin in particular), but more accessible (hardware purchases are typically not needed) and not as much wasteful. Therefore Ercoin community hopes to attract both people that are tired of ICO projects and people that are discouraged by wastefulness and environmental impact of cryptocurrencies that use proof of work as a method of distribution. CO₂ emission savings during the IBO can be estimated using an [IBO visualization tool](https://ibo.laboratorium.ee).

Besides distribution, Ercoin aims to distringuish itself by a unique set of technical and economical features. **Delegated proof of locked stake** is used as a consensus algorithm. This is a variant of proof stake in which voting power is proportional not only to the amount of coins owned, but also to the time for which the funds are locked (with a time cap applied). The purpose of this solution is to **emphasize the role of long-term investors** in decision making. Investors don’t need to perform validation themselves, but can instead delegate this duty to professional **validator service providers**.

There will be **no inflation** in Ercoin and therefore no arbitrary inflation tax. Costs of maintaining the network will be covered from fees. **Fee amounts will be decided by the consensus of validators.**

In place of smart contracts, Ercoin proposes **transaction messages** and **accounts managed by multiple keys in arbitrary combinations**. It is believed that these two features will cover many real-life applications like facilitation of automated services and private arbitration (via deposits managed by 2-of-3 signatures).

Besides Ercoin-specific features, there is a number of advantages coming from the **usage of Tendermint**, like **fast confirmations** and partially standarized API. Overall, Ercoin aims to be a cryptocurrency which is fairly distributed, efficient, easy to use, easy to integrate, useful in common business cases and also provides a sound economic model, contributing to the long term cryptocurrency value.
