---
title: Ercoin 1.0.0 beta 1
date: 2019-10-08
---

Ercoin 1.0.0 beta has been released. This version brings the [announcement of Initial Burn Offering](/news/ibo.html) which will be used to obtain initial distribution. See the linked announcement for detailed rules and instruction. We are also pleased to announce [bug bounty programme](/news/bug-bounty-1.0.0-beta.1.html). For [Ercoin story](https://jurewicz.org.pl/en/blog/2019/10/08/ercoin-story/), see the linked blog post.

Below is a selective list of changes since the last release. There are no plans to make any backwards incompatible changes before releasing the final 1.0.0 version, but this possibility cannot be completely excluded.

* Support for easy (non-compact) m-of-n signatures. This changes format of all transactions.
* Change Merkle path format to be easier to generate and process.
* Automatic account validity management (including removal of account transaction type).
* Change base unit to microercoin.
* Treat vote transactions containing old votes as invalid.
* Fix and update the way genesis data is calculated.
