---
title: Pre-production bug bounty
date: 2019-10-08
---
With the [announcement of version 1.0.0 beta](/news/ercoin-1.0.0-beta.1.html), we have a bug bounty programme to announce! The goal is to find bugs before Ercoin goes into production.

## Bug categories

### Double spending

Reward: **$1000**

Construct transactions A and B such that:

* A is a valid transaction;
* B can be derived from A without a complete set of B’s signing keys (note that A can be equal to B);
* In some circumstances, B can be successfully executed after A.

### Unauthorized spending

Reward: **$1000**

Given an insufficient set os signing keys, construct a transaction that, in some circumstances, can be successfully executed.

### Node crash

Reward: **$350**

Construct a transaction that, under some circumstances, causes the node to crash either on `CheckTx` or on `DeliverTx`. Note: the crash needs to be caused by the transaction itself, not by some pathological application state which is unlikely to happen in production (like fee votes absent or all being equal to zero).

## Rules

The bounties are mostly funded ($750 + $750 + $350) by Ergologic, Inc. Orlando, Florida, USA.

Each bug will be assigned to just one category. The total amount of rewards is limited. Bounties will be processed in the order of submission. After each recognition, it will be announced how it affects the budget and payable bug categories.

Bug cause must lay in the ABCI server. Note however that ABCI client (Tendermint) has [its own bug bounty programme](https://hackerone.com/tendermint).

Bounties will be paid in Bitcoin. When converting USD to BTC, the price of Bitcoin will be the closing price from the day the bounty was acknowledged, as recorded on [Coinpaprika](https://coinpaprika.com/).

To claim a bounty, open an issue in [the repository of Ercoin’s ABCI server](https://gitlab.com/Ercoin/ercoin) before 13th of December 2019 (UTC) and provide your payment address.

Bug categories and the above rules are subject to change, providing reasonable transparency.
