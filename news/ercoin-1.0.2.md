---
title: Ercoin 1.0.2
date: 2023-02-06
---

Ercoin 1.0.2 has been released. This version adds support for a situation when there are no voters for the next set of validators, in which case the current set of validators is extended onto the next epoch. Since this is a pathological case, we treat it as an emergency workaround to let the chain run, expecting the normal voting process to be resumed.

All nodes need to upgrade.
