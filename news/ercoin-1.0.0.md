---
title: Ercoin 1.0.0 and the launch
date: 2020-01-22
---

Ercoin 1.0.0 has been released. The launch of the main network is scheduled to happen on 2020-01-23 00:00:00 UTC.

The initial proposed launch date of 2019-12-15 00:00:00 UTC was delayed because of some IBO-related issues and also to wait for the release of Tendermint 0.33. See the issues [#8](https://gitlab.com/Ercoin/ercoin/issues/8), [#9](https://gitlab.com/Ercoin/ercoin/issues/9) and [#10](https://gitlab.com/Ercoin/ercoin/issues/10) in the main repository for details.
