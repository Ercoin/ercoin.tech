---
title: Questions & answers
---

## General

What is interesting about Ercoin?

: Read [“Reasons to be interested in Ercoin”](/reasons.html).

## Initial Burn Offering

Is IBO similar to ICO?

: Not really. It is a facetious name for distribution by proof of burn.

What will happen with the funds collected during the IBO?

: Those funds will not be collected, they will be destroyed. (Similarly, there is no electricity “collected” during Bitcoin mining).

## Design

How does paying for accounts work?

: Each account is valid until some time in future. When that time comes, a fee is deducted to extend the validity for a fixed period (30 days at the time of writing). If there is no sufficient balance to cover the full period, partial extension is performed. If balance is zero, the account is expired.

How can I track account fees?

: Account extensions are recorded as events (with type `account.extended`) in corresponding blocks. At the time of writing, it is not possible to search blocks by events (see [issue #3665 in Tendermint](https://github.com/tendermint/tendermint/issues/3665) for details). When this is resolved, wallets will be able to query for account fee payments.

What is the difference between *delegated* proof of stake and ordinary proof of stake?

: Delegated proof of stake means that capital holders cannot create blocks themselves, but instead vote to elect a set of well-known validators that is responsible for creating blocks.

What is the difference between proof of *locked* stake and ordinary proof of stake?

: In ordinary proof of stake, voting/staking power is proportional to the capital. In proof of locked stake, it is proportional to the capital and also the amount of time for which the funds will be locked (in Ercoin a time cap is applied).

What are the benefits of proof of *locked* stake?

: Long term investors, more interested in long term cryptocurrency value, are more decisive.

What is a locked account?

: Locked account is an account from which no transfer or burn transactions can be performed. In exchange, it gains a privilege of voting for validators.

What is a validator?

: Validator is a network node that has a duty of creating blocks (together with other validators). Validators are elected by locked stakeholders.

How does the “fair voting system” work?

: Seats are assigned proportionally to the votes, with fractional seats being assigned in a randomized way.

How do validators come to consensus on fees?

: Validators vote for preferred fee amounts per fee category, with results chosen as medians of fee votes weighted by voting power, with higher middle values chosen if sum of weights is even.

I want to know more about the design and implementation, what should I read?

: You may want to start with [Tendermint documentation](https://tendermint.com/docs/) to get an overview of how Tendermint-based blockchain applications work. [Technical documentation in the main Ercoin repository](https://gitlab.com/Ercoin/ercoin/blob/master/README.md#technical-documentation) describes the ABCI server, but does not cover all design choices and technical details. For these, you may want to inspect the source code in that repository or ask questions on the community channels.
