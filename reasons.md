---
title: Reasons to be interested in Ercoin
---
This list includes reasons (sometimes overlapping) to be interested in Ercoin and not other cryptocurrencies. Reasons for being interested in cryptocurrencies in general are not covered here. Reasons related to technical internals of Ercoin are also currently not covered.

1. **Fair distribution.** No entity will be privileged.
1. **Environmental friendliness.** Neither the distribution nor daily operation will involve excessive energy consumption (and therefore CO₂ emission).
1. **Innovation.** Ercoin operates using 100% delegated proof of locked stake, is fairly distributed, has expirable paid accounts and dynamic transaction fees decided by consensus. If you are aware of an older cryptocurrency with this combination of features, let us know, so we can remove this bullet point.
1. **Delegated proof of locked stake.** Voting power of capital holders is proportional not only to the capital itself, but also to the time for which it is locked (with a time cap). Therefore long-term investors, who are more interested in long-term cryptocurrency value, are more decisive.
1. **Initial Burn Offering.** This method of distribution is similar in spirit to the method used in Bitcoin and many other cryptocurrencies, but better in the sense that it doesn’t need high energy-consuming computations and specialized hardware.
1. **Fast confirmations.** Since the set of validators is well known, they are able to create blocks very fast, so typically you will confirm your transaction in a few seconds.
1. **Simplicity.** No advanced scripting language, not to mention no Turing-complete computations. Ercoin aims to be simple enough for end user to not require a PhD in computer science, while still enabling some advanced use cases.
1. **Transaction messages.** Want to describe your transfer? Want to associate a checksum of a contract with a transfer so that it can be used in case of a dispute? Want to easily implement automation around transfers, without the need to create a separate address for each customer or each purchase? Transaction messages may be a great solution for you.
1. **No inflation.** The network is financed using account fees and transaction fees, so there is no need to apply an “inflationary tax”.
1. **Accounts instead of UTXOs.** Money is stored using accounts instead of unspent transaction outputs, which is often more natural and simpler to use. For example, for each transaction there exists a sender address.
1. **Advanced account management.** It is possible to create accounts that are managed by multiple keys in arbitrary combinations (known in advance).
1. **Arbitration friendliness.** Want to pay for a service, but are afraid that it will not be performed? Want to perform a service, but are afraid if you will be paid? Send payment deposit to an account that is managed by any 2 of the following 3: payer, payee and trusted third party. If everything goes well, payer and payee jointly create a payment transaction. If there is a dispute, trusted third party is involved.
1. **Expirable paid accounts.** Accounts pay fees for being stored on the blockchain, which prevents bloat.
1. **Low-cost operation.** The consensus algorithm used in Ercoin does not involve computational race just for the sake of decision making. The duty of creating blocks is constrained to a set of validators (chosen in election).
1. **Final confirmations.** Creating a block requires signatures of at least 2⁄3 of validators. If near the time of block creation at least 1⁄3 of validators are not acting maliciously, concurrent blocks will not be created. Therefore in a typical situation it is not necessary to ask “How many confirmations do you have?” — 1 confirmation means that a transaction will not be orphaned. Note that this reasoning does not include long range attacks.
1. **Fair voting system.** Even with a smallest vote backing, you have a chance to become a validator, thanks to partial randomness.
1. **Original code.** Though Ercoin uses [Tendermint](https://tendermint.com) as its consensus engine, its business logic is original. We are not a clone.
